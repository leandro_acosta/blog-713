<?php

use App\Models\User;

class SentrySeeder extends Seeder {

    public function run()
    {
        Eloquent::unguard();

        // trunca las tablas de sentry
        DB::table('users')->delete();
        DB::table('groups')->delete();
        DB::table('users_groups')->delete();

        // crea un usuario (el tuyo) (***)
        Sentry::getUserProvider()->create(array(
            'email'       => 'leacosta97@gmail.com',
            'password'    => "1234",
            'first_name'  => 'Leandro',
            'last_name'   => 'Acosta',
            'activated'   => 1,
        ));

        // crea un usuario editor para demostrar diferentes usuarios en diferentes grupos
        Sentry::getUserProvider()->create(array(
            'email'       => 'editor@gmail.com',
            'password'    => "1234",
            'first_name'  => 'Leandro',
            'last_name'   => 'Editor',
            'activated'   => 1,
        ));

        // Crea un grupo, administradores en este caso
        Sentry::getGroupProvider()->create(array(
            'name'        => 'Admin',
            'permissions' => array('admin' => 1),
        ));

        // Creamos otro grupo de editores        
        Sentry::getGroupProvider()->create(array(
            'name'        => 'Editor',
            'permissions' => array('editor' => 1),
        ));

        // Asignamos grupo a cada usario, en este caso mi usuario será admin
        $user  = Sentry::getUserProvider()->findByLogin('leacosta97@gmail.com');
        $group = Sentry::getGroupProvider()->findByName('Admin');
        $user->addGroup($group);

        // Asignamos grupo al editor, el editor estará en el grupo de editores
        $user  = Sentry::getUserProvider()->findByLogin('editor@gmail.com');
        $group = Sentry::getGroupProvider()->findByName('Editor');
        $user->addGroup($group);

    }

}