<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsFiles extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('posts_files', function($table)
		{
			$table->increments('id');
			$table->integer('parent_id');
			$table->string('name');
			$table->string('title');
			$table->string('caption');
			$table->integer('file_size');
			$table->string('mime_type');
			$table->integer('sorted');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
