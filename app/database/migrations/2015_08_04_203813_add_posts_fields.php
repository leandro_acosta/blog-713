<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPostsFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('posts', function($table)
		{		
			$table->string('slug');
			$table->integer('user_id');
			$table->integer('status_id');
			$table->integer('home');
			$table->integer('comments');
			$table->datetime('publish_start');
			$table->datetime('publish_end');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
