@extends('layouts.default')

@section('content')

<h1>{{ $post->title }}</h1>
<blockquote>{{ $post->caption }}</blockquote>
<p>{{ $post->body }}</p>

@stop
