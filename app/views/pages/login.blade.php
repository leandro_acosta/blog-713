@extends('layouts.default')

@section('content')

<div class="col-md-6">
  <form class="form" method="POST" action="{{ URL::route('post.login') }}">

  @if ($errors->has('login'))
    <div class="alert alert-danger alert-dismissable">{{ $errors->first('login', ':message') }}</div>
  @endif        

    <h3 class="form-signin-heading">Ingresa con tu email y contraseña</h3>
    <input type="email" name="email" class="form-control" placeholder="Tu email" required autofocus>
    <input type="password" name="password" class="form-control" placeholder="Tu contraseña" required>

    <label class="checkbox">
      <input type="checkbox" name="remember" value="remember-me"> Recuerda mis datos
    </label>

    <input type="submit" class="btn btn-large btn-primary" value="Ingresar">
  </form>     
</div>

@stop