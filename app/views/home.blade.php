@extends('layouts.default')

@section('content')

<div class="row">
<div class="slick slick-home">
@foreach($posts as $post)
    <div class="cover" style="background-image:url(/uploads/posts/{{ FileHelper::get_resized($post->image->name,'hd') }})">
        <a href="/posts/{{ $post->id }}">
            <div class="col-md-8">
                <h1>{{ $post->title }}</h1>
                <h4>{{ $post->caption }}</h4>
            </div>
        </a>
    </div>
@endforeach
</div>
</div>
@stop