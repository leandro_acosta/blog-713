@extends('layouts.admin')

@section('content')

<h1 class="text-success"><i class="ion-compose"></i> {{ $post_count }}</h1>
<h1 class="text-info"><i class="ion-person"></i> {{ $user_count }}</h1>

@stop