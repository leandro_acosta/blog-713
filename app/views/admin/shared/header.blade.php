<nav class="navbar" role="navigation">
  <div class="container-fluid">
    <div class="navbar-header pull-left">
      <a href="/admin" class="navbar-brand">
        <i class='ion-leaf x2 pull-left'></i> 
        <div class="pull-left text-left text">
          <strong>Laravel</strong>
          <small>  </small>
        </div>
      </a>
    </div>

    <ul class="nav navbar nav-pills pull-right"> 
      <li>
          <a href="/admin/posts" title="" data-placement="bottom">
            <i class='ion-compose x2'></i>
          </a>
      </li>          
      <li>
          <a href="/admin/users" title="" data-placement="bottom">
            <i class='ion-person x2'></i>
          </a>
      </li>          
      <li>
          <a href="/logout" title="" data-placement="bottom">
            <i class='ion-log-out x2'></i> {{ Sentry::getUser()->first_name }}
          </a>
      </li>          
    </ul>
  </div>
</nav>