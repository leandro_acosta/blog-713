@extends('layouts.admin')

@section('content')

<div class="col-md-12">

    <h2><i class="fa fa-rss"></i> &nbsp; Noticias</h2>

    <!-- Breadcrumb -->
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="icon-home"></i> Dashboard</a></li>
        <li><a href="/admin/posts" class="bread-current">Noticias</a></li>
        <li class="active">{{ $entry->title }}</li>
    </ol>

    <div class="clearfix"></div>

    <h6>Última actualización {{ $entry->updated_at }}</h6>
    <hr />

    {{ Form::model($entry, array('method' => 'put', 'files' => true, 'route' => array('admin.posts.update', $entry->id), 'class' => 'form', 'id' => "edit_form")) }}

    <div class='col-md-9'>
        <div class="row">

          <div class="form-group">
              <label class="control-label">
                {{ Form::label('title', 'Título') }}
              </label>
              <div class="controls">
                  {{ Form::text('title', $entry->title, array('class' => "form-control input-xxlarge",'placeholder' => "Un buen título siempre es importante")) }}
              </div>
          </div>

          <hr />
          <h6>Extracto</h6>
          {{ Form::textarea('caption', $entry->caption, array('class' => "form-control")) }}

          <hr />

          <div class="progress summernote-progress hide">
            <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
              <span class="sr-only"></span>
            </div>
          </div>        

          <h6>Contenido</h6>
          {{ Form::textarea('body', $entry->body, array('class' => "form-control summernote")) }}
        </div>
    </div>


    <div class="col-md-3">
      <div class="form-group">
          <label class="control-label">
            {{ Form::label('status_id', 'Estado') }}
          </label>
          <div class="controls">
              <input name="status_id" type="checkbox"{{ $entry->status_id ? ' checked' : '' }} />
          </div>
      </div>

      <hr>

      <div class="form-group">
          <label class="control-label">
            {{ Form::label('home', 'Portada') }}
          </label>
          <div class="controls">
            <div class="toggle-button">
              <input name="home" type="checkbox"{{ $entry->home ? ' checked' : '' }} />
            </div>     
          </div>
      </div>                    

      <hr>

      <div class="form-group">
          <label class="control-label">
            {{ Form::label('comments', 'Comentarios de Disqus') }}
          </label>
          <div class="controls">
            <div class="toggle-button">
              <input name="comments" type="checkbox"{{ $entry->comments ? ' checked' : '' }} />
            </div>     
          </div>
      </div>    

      <hr/>

      <div class="form-group">
        <label class="control-label">
          {{ Form::label('publish_start', 'Comienza') }}
        </label> 
        <div class="form-group">
          <div class='input-group date datetimepicker'>
              <input type='text' class="form-control" id="publish_start" value="" />
              <span class="input-group-addon"><span class="fa fa-calendar"></span>
              </span>
          </div>
        </div>
      </div>

      <div class="form-group">
        <label class="control-label">
          {{ Form::label('publish_end', 'Finaliza') }}
        </label> 

        <div class="form-group">
          <div class='input-group date datetimepicker'>
              <input type='text' class="form-control" id="publish_end" value="" />
              <span class="input-group-addon"><span class="fa fa-calendar"></span>
              </span>
          </div>
        </div>
      </div>  
      <hr/>
      <input type="hidden" name="id" value="{{ $entry->id }}">
        <div class="form-group tags-list">
            <label class="control-label">tags</label>

            <div class="input-group">
              <input type="text" id="tags-add" class="form-control" placeholder="Tema 1, Tema 2" />
              <span class="input-group-btn">
                <button type="button" class="btn btn-default btn-primary btn-tags-add"><i class="ion-pricetag"></i></button>
              </span>
            </div>
          <div class="tags-included"></div>
          <div class="tags-excluded"></div>
        </div>            
      </div>            
    </div>  

    {{ Form::close() }}

    <div class="clearfix"></div>
          <hr />

          <h6>Galería Multimedia</h6>

          <!-- Imágenes del Vehículo -->
          <div class="row">
            <div class="col-md-9">
              <div class='group-control'>&nbsp;</div>

              <form action="{{ URL::route('image.upload') }}" id="form_dropzone" class="dropzone" data-url="/json/post/images.json" data-id="{{ $entry->id }}" data-tag="posts" data-scheme="PostImage">
              </form>

              <div class='clearfix'></div>
            </div>
            <div class="col-md-3">
              <div class='group-control'>&nbsp;</div>
              <div class="list-group">
                  <div class="mgbottom20 ventaja">
                      <h4 class="mg0 strong"><i class='fa fa-check'></i>&nbsp;Imágenes del Artículo</h4>
                      <h6>Arrastra tu carpeta o grupo de archivos al <em>zona de arrastre</em></h6>
                      <p>Asegúrate de que las imágenes sean representativas y de al menos resolución media 640x480px, de lo contrario.</p>
                  </div>
              </div>
            </div>
          </div>
    <div class='navbar navbar-fixed-bottom'>
      <a href="{{ URL::route('admin.posts.index') }}" class="btn"> <i class='fa fa-reply'></i>&nbsp; Volver al listado </a>
      <button class="btn btn-primary" onclick="$('#edit_form').submit();"><i class='fa fa-check-square'></i>&nbsp; Guardar</button>
    </div>

</div>

@stop