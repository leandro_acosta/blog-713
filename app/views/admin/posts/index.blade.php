@extends('layouts.admin')

@section('content')

@if( Session::has( 'error' ))
    <div class="alert alert-danger" role="alert">{{ Session::get( 'error' ) }}</div>
@elseif( Session::has( 'warning' ))
    <div class="alert alert-warning" role="alert">{{ Session::get( 'warning' ) }}</div>
@elseif( Session::has( 'success' ))
    <div class="alert alert-success" role="alert">{{ Session::get( 'success' ) }}</div>
@endif     

<a href="/admin/posts/create" class="btn btn-lg btn-success">Nuevo</a>
<hr>
<table class="table">
    <tr>
        <th width="1%">#</th>
        <th>Estado</th>
        <th>Título</th>
        <th>Palabras</th>
        <th>Modificado</th>
        <th width="15%"><i class="ion-gears"></i></th>
    </tr>
@foreach($posts as $entry)
    <tr>
        <td>{{ $entry->id }}</td>
        <td><i class="x2 ion-{{ $entry->status_id ? 'checkmark-circled text-success' : 'android-cancel text-danger' }}"></i></td>
        <td>{{ $entry->title }}</td>
        <td>{{ str_word_count($entry->body) }}</td>
        <td>{{ $entry->updated_at }}</td>
        <td>
            {{ Form::open(array('route' => array('admin.posts.destroy', $entry->id), 'method' => 'delete')) }}
                <a class="btn btn-success" href="{{ URL::route('admin.posts.edit', $entry->id) }}">
                    <i class="ion-edit"></i>
                </a>
                  <button type="submit" onclick="if(!confirm('¿Estás seguro?')){return false;}" href="{{ URL::route('admin.posts.destroy', $entry->id) }}" class="btn btn-danger"><i class="ion-trash-b"> </i></button>
            {{ Form::close() }}
        </td>
    </tr>
@endforeach
</table>

{{ $posts->links() }}

@stop