@extends('layouts.admin')

@section('content')


<a href="/admin/posts" class="btn btn-lg btn-success" title="Listado de Articulos">
	<i class="ion-compose"></i> {{ $post_count }}
</a>



<a href="/admin/users" class="btn btn-lg btn-warning" title="Listado de usuarios">
	<i class="ion-person"></i> {{ $user_count }}
</a>

@stop