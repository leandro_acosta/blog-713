<!DOCTYPE html>
<html lang="es">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>Taller de Laravel</title>

  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Taller de Laravel">
  <meta name="keywords" content="laravel, framework, taller">
  <meta name="author" content="Escuela 713">

  <!-- Stylesheets -->
  <link type="text/css" rel="stylesheet" href="/assets/css/bootstrap.min.css">
  <link type="text/css" rel="stylesheet" href="/assets/css/ionicons.css">
  <link type="text/css" rel="stylesheet" href="/assets/css/slick.css">
  <link type="text/css" rel="stylesheet" href="/assets/css/slick-theme.css">
  <link type="text/css" rel="stylesheet" href="/assets/css/font-awesome-animation.css">
  <link type="image/x-icon" rel="shortcut icon" href="/favicon.ico">

  <link type="text/css" rel="stylesheet" href="/assets/css/theme.css">

  <!-- Javascripts -->
  <script type="text/javascript" src="/assets/js/jquery-2.1.3.min.js"></script>
  <script type="text/javascript" src="/assets/js/slick.min.js"></script>
  <script type="text/javascript" src="/assets/js/app.js"></script>



</head>
<body>

  @include('shared/header')
  <div class="container">
    <div class="content">
      @yield('content')
    </div>
  </div>

  @include('shared/footer')

</body>
</html>