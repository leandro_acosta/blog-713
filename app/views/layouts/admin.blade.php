<!DOCTYPE html>
<html lang="es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Admin</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="author" content="Motorsom">

<!-- Stylesheets -->
<link href="/assets/css/bootstrap.min.css" rel="stylesheet">
<link href="/assets/css/ionicons.css" rel="stylesheet">
<link href="/assets/css/font-awesome.min.css" rel="stylesheet">
<link href="/assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
<link href="/assets/css/summernote.css" rel="stylesheet">
<link href="/assets/css/chosen.css" rel="stylesheet">
<link href="/assets/css/dropzone.css" rel="stylesheet">
<link href="/assets/css/admin.css" rel="stylesheet">
<link rel="shortcut icon" href="/favicon.ico">

<script src="/assets/js/jquery-2.1.3.min.js"></script>
<script src="/assets/js/jquery-ui-1.10.4.custom.min.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/summernote.js"></script>
<script src="/assets/js/dropzone.js"></script>
<script src="/assets/js/moment-with-locales.js"></script>
<script src="/assets/js/bootstrap-datetimepicker.js"></script>
<script src="/assets/js/admin/app.js"></script>

</head>
<body>

@include('admin.shared.header')
<div class="container">
  <div class="content">
    @yield('content')
  </div>
</div>

@include('admin.shared.footer')

</body>
</html>
