@extends ('layouts.default')

@section('content')

<h1>Resultados de la busqueda {{ Input::get('s') }}</h1>

@foreach($posts as $post)
	<h3><a href="/posts/{{ $post-id }}">{{ $post-title }}</a></h3>
	<p>{{ $post->caption }}</p>
	<hr>
@endforeach

{{ $posts->links()}}

@stop