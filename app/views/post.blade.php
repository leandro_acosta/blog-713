@extends('layouts.default')

@section('content')
<div class="col-md-9">
	<h1>{{ $post->title }}</h1>
	<blockquote>{{ $post->caption }}</blockquote>

	@if($post->images->count())
	<div class="slick-single slick-slider">
	@foreach($post->images as $is => $image)
		@if( is_file( 'uploads/posts/' . FileHelper::get_resized($image->name, 'wd')))
			<div class="img-rounded carousel-img" style="width:900px; height:500px;background:url(/uploads/posts/{{ FileHelper::get_resized($image->name, 'wd') }}) no-repeat;"></div>
		@endif
	@endforeach
	</div>
	<div class="slick-nav slick-slider">
		@foreach($post->images as $i => $image)
			@if ( is_file('uploads/posts/' . FileHelper::get_resized($image->name,'th')))
			<div class="hand"><img src="img-thumbnail" data-lazy="/uploads/posts/{{ FileHelper::get_resized($image->name,'th') }}"/></div>
		@endif
		@endforeach
	</div>
	@endif

	<p>{{ $post->body }}</p>

	@if ($post->tags->count())
		<span>Temas:</span>&nbsp;
		@foreach($post->tags as $tag)
			<a href="/tags/{{ $tag->tag->tag }}"><span class="label label-success">{{ $tag->tag->tag }}</span></a>
		@endforeach
	@endif

	<div class="clearfix"></div>
</div>

<div class="col-md-3">
	@include('shared.sidebar')
</div>

@stop