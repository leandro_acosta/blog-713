<div class="sidebar">
	<div class="list-group">
		<h5>Buscar en el <em>blog</em></h5>
		<form id="searchform" action="{{ URL::route('search') }}" class="form-search" method="GET">
			<div class="input-group">
				<input class="form-control" type="text" value="{{ Input::get('s') }}" name="s" placeholder="Buscar" required />
				<span class="input-group-btn">
					<button class="btn btn-info" type="submit">
						<i class="fa fa-search"></i>
					</button>
				</span>
			</div>		
		</form>
	</div>
</div>