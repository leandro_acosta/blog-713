<nav class="navbar" role="navigation">
  <div class="container-fluid">
    <div class="navbar-header pull-left">
      <a href="/" class="navbar-brand">
        <i class='ion-leaf x2 pull-left'></i> 
        <div class="pull-left text-left text">
          <strong>Laravel</strong>
          <small></small>
        </div>
      </a>
    </div>

    <ul class="nav navbar nav-pills pull-right"> 
      <li>
          <a href="/login" title="" data-placement="bottom">
            <i class='ion-person x2'></i>
          </a>
      </li>          

      <li>
          <a href="/posts" title="" data-placement="bottom">
            <i class='ion-compose x2'></i>
          </a>
      </li>          
      <li>
          <a href="/contact" title="Contacta con nosotros" data-placement="bottom">
            <i class='ion-android-mail x2'></i>
          </a>
      </li>
    </ul>
  </div>
</nav>