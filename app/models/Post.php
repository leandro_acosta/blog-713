<?php namespace App\Models;

class Post extends \Eloquent {

    protected $table = 'posts';

    public function author()
    {
        return $this->belongsTo('User','user_id');
    }
    public function images()
	{
	    return $this->hasMany('PostImage','parent_id')
	        ->orderBy('sorted');
	}

	public function image()
	{
	    return $this->hasOne('PostImage','parent_id')
	        ->orderBy('sorted');
	}
	
	public function tags()
	{
		return $this->hasMany('PostTag','post_id')
			->orderBy('id');
	}

	public static function visible()
	{	
		$date = date('Y-m-d H:i:s');
		return static::whereRaw("status_id = 1 and ((publish_start < ? and publish_end > ?) or (publish_start < ? and publish_end = '0000-00-00 0000-00-00'))", [$date, $date,$date]);
	}

}
