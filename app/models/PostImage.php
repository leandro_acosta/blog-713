<?php namespace App\Models;

class PostImage extends \Eloquent {

    protected $table = 'posts_files';

    public function post()
    {
        return $this->hasOne('Post');
    }
}