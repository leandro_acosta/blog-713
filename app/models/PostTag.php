<?php namespace App\Models;

class PostTag extends \Eloquent {

    protected $table = 'posts_tags';

    public function tag()
    {
        return $this->belongsTo('Tag');
    }

}