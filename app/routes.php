<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::group(array('prefix' => 'admin', 'before' => 'auth.admin'), function()
{
 	Route::any('/',                 'App\Controllers\Admin\DashController@index');
	Route::resource('posts',        'App\Controllers\Admin\PostController');
	Route::resource('users',        'App\Controllers\Admin\UserController');
	Route::post('tags_relation_add',  array('uses' => 'App\Controllers\Admin\PostController@tags_relation_add'));
	Route::post('tags_relation_remove',  array('uses' => 'App\Controllers\Admin\PostController@tags_relation_remove'));
	Route::post('tags_all',  array('uses' => 'App\Controllers\Admin\PostController@tags_all'));
});

Route::post('login',  array('as' => 'post.login', 'uses' => 'App\Controllers\AuthController@login'));
Route::get('logout',  array('uses' => 'App\Controllers\AuthController@logout'));
Route::get('/', array('uses' => "App\Controllers\PostController@home"));
Route::get('/posts', array('uses' => "App\Controllers\PostController@index"));
Route::get('/posts/{id}', array('uses' => "App\Controllers\PostController@show"));
Route::get('/{slug}', array('uses' => "App\Controllers\PageController@show"));

Route::post('image/upload',               array('as' => 'image.upload',     'uses' => 'App\Controllers\ImageController@upload'));
Route::post('image/destroy',              array('as' => 'image.destroy',     'uses' => 'App\Controllers\ImageController@destroy'));
Route::post('image/ordering',             array('as' => 'image.ordering',     'uses' => 'App\Controllers\ImageController@ordering'));
Route::get('json/{path?}',                array('uses' => 'App\Controllers\JsonController@show'))
      ->where('path', '.+');



Route::get('search', array('as' => 'search',     'uses' => 'App\Controllers\PostControll er@search'));



Route::get('tags/{tag}', array('as' => 'tags',    		'uses' => 'App\Controllers\PostController@tags'));
