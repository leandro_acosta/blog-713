<?php namespace App\Controllers\Helpers;

class DateHelper extends \DateTime {

    static function timespan($mins) {
        $x3="";
        $mins = round($mins);
        if($mins==0){
            $x3="segundos";
        } elseif($mins > 40319){ // meses
            $ratio = $mins / 40320 ;
            $d = round($ratio);
            $s = $d > 1 ? "es":"";
            $x3 = $d . " mes".$s;
        } elseif($mins > 10079 && $mins < 40319){ // semanas
            $ratio = $mins / 10080 ;
            $d = round($ratio);
            $s = $d > 1 ? "s":"";
            $x3 = $d . " semana".$s;
        } elseif($mins > 1439 && $mins < 10079){ // dias
            $ratio = $mins / 1440 ;
            $d = round($ratio);
            $s = $d > 1 ? "s":"";
            $x3 = $d . " día".$s;
        } elseif($mins > 59 && $mins < 1439){ // horas
            $x3 = DateHelper::min2hour(round($mins));
        } else {
            $s = $mins > 1 ? "s":"";
            $x3 = $mins . " minuto".$s;
        }

        return $x3;
    }

    static function min2hour($mins) { 
        if ($mins < 0) { 
            $min = Abs($mins); 
        } else { 
            $min = $mins; 
        } 
        $H = Floor($min / 60); 
        $M = ($min - ($H * 60)) / 100; 
        $hours = $H +  $M; 
        if ($mins < 0) { 
            $hours = $hours * (-1); 
        } 
        $expl = explode(".", $hours); 
        $H = $expl[0]; 
        if (empty($expl[1])) { 
            $expl[1] = 00; 
        } 
        $M = $expl[1]; 
        if (strlen($M) < 2) { 
            $M = $M . 0; 
        }

        $hours = $H;

        if($M > 0 && $H < 3)
            $hours.= ":" . $M;

        $s = ($H > 1 || $M > 1) ? "s":"";
        $hours.= " hora".$s; 

        return $hours; 
    } 


    static function ts2date($d){
        if(substr($d,2,1)=="-")    {
            $mk = mktime(substr($d,11,2),(int)substr($d,14,2),(int)substr($d,17,2),(int)substr($d,3,2),(int)substr($d,0,2),(int)substr($d,6,4));
        } else {
            $mk = mktime(substr($d,11,2),(int)substr($d,14,2),(int)substr($d,17,2),(int)substr($d,5,2),(int)substr($d,8,2),(int)substr($d,0,4));
        }
        return $mk;
    }

    static function timespan_date($date,$bypass = 0, $format="j M"){

        $y = explode("-",$date);

        if($y[0]!=date('Y')){
            $format.=" Y";
        }

        if(strpos($date,'-')||strpos($date,' ')){
            $date = DateHelper::ts2date($date);
        }

        $mins = (time() - $date) / 60;
        $string = "";

        if($mins > 150000 && ! $bypass){
            $str = date($format,$date);
        } else {
            $str = DateHelper::timespan($mins);
        }

        return $str;
    }    

    static function date_fmt(){return SITE_DATEFORMAT ? SITE_DATEFORMAT : 'd M H:i';}

}