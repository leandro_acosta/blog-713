<?php namespace App\Controllers\Helpers;

use Illuminate\Filesystem\Filesystem;

class StrHelper extends \DateTime {

    static function words($words){

        $wcount =  str_word_count($words);

        $ret = '';
        $words_pmin = 200;

        $estread = ceil($wcount / $words_pmin);
        $ret.= "" . $wcount . " palabras &mdash; \n";
        $ret.= "" . $estread . " minuto";
        $ret.= $estread > 1 ? "s":"";

        return $ret;
    }

    public static function sanitize($name,$folder = 'posts')
    {
        $safe = $name;
        $safe = str_replace("#", "Nro", $safe);
        $safe = str_replace("$", "Dollar", $safe);
        $safe = str_replace("%", "Percent", $safe);
        $safe = str_replace("^", "", $safe);
        $safe = str_replace("&", "and", $safe);
        $safe = str_replace("*", "", $safe);
        $safe = str_replace("?", "", $safe);
        $safe = str_replace("(", "", $safe);
        $safe = str_replace(")", "", $safe);

        $path = base_path().'/public';
        $dir = $path . '/uploads/' . $folder;

           $fs = new Filesystem();
        $files = $fs->files($dir);
        sort($files);
        $j=1;

        while(in_array($safe,$files))
        {
            $safe= preg_replace("/\((.*?)\)/", "",$safe);
            $parts= explode(".",$safe);
            $parts2= $parts;
            unset($parts2[count($parts2)-1]);
            $safe= implode(".",$parts2) . "($j)." .  $parts[count($parts)-1];
            $j++;
        }

        return $safe;
    }
    static function nice_size($fs){if ($fs >= 1073741824) $fs = round(($fs / 1073741824 * 100) / 100).' Gb'; elseif ($fs >= 1048576) $fs = round(($fs / 1048576 * 100) / 100).' Mb'; elseif ($fs >= 1024) $fs = round(($fs / 1024 * 100) / 100).' Kb';else $fs = $fs .' b';return $fs;}

    static function credits2euro($credits = 0, $comma = true){
        $dinero = (float) Option::where('option_key','credit_euro_' . $credits)
            ->pluck('option_value');

        if(! $dinero ){
            $dinero = (float) Option::where('option_key','credit_euro')
                ->pluck('option_value');
        }

        $formateado = sprintf("%01.2f", $dinero * $credits);
        //$formateado = round(sprintf("%01.2f", $dinero * $credits),2);

        if( $comma ){
            $formateado = str_replace(".",",",$formateado);
        }

        return $formateado;
    }

    static function bold($str, $keywords = '')
    {
        $keywords = preg_replace('/\s\s+/', ' ', strip_tags(trim($keywords))); // filter

        $style = 'bold';
        $style_i = 'bold-strong';

        /* Apply Style */

        $var = '';

        foreach(explode(' ', $keywords) as $keyword)
        {
            $replacement = "<span class='".$style."'>".$keyword."</span>";
            $var .= $replacement." ";

            $str = str_ireplace($keyword, $replacement, $str);
        }

        /* Apply Important Style */

        $str = str_ireplace(rtrim($var), "<span class='".$style_i."'>".$keywords."</span>", $str);

        return $str;
    }

  static function getCColor( $credits  ){
        $color = 'grey';

        if($credits > 0 && $credits < 100) {
            $color = 'blue';
        } else if($credits > 0 && $credits < 500) {
            $color = 'green';
        } else if($credits > 0 && $credits < 1000) {
            $color = 'magenta';
        }

        return $color;

    }



  static function get_public_id( $id )
  {
    return date('y') . sprintf("%04s", $id);
  }
  static function get_id_from_public_id( $id )
  {
    return (int) substr($id, -4);
  }    
}