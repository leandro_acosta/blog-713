<?php namespace App\Controllers\Helpers;

use Input, Image;

class FileHelper extends \DateTime {
    static $config = array(
        //'mini' => "45x34",
        'th'     => "130x130",
        'bn'     => "270x130",
        'cs'     => "925x175",
        'wd'     => "900x500",
        'hd'     => "1024x768"
    );

    static function file_extension($filename){    $dot= substr(strrchr($filename, "."), 1);$str= explode("?",$dot);return $str[0];}

    static function nice_size($fs){if ($fs >= 1073741824) $fs = round(($fs / 1073741824 * 100) / 100).' Gb'; elseif ($fs >= 1048576) $fs = round(($fs / 1048576 * 100) / 100).' Mb'; elseif ($fs >= 1024) $fs = round(($fs / 1024 * 100) / 100).' Kb';else $fs = $fs .' b';return $fs;}

    static function is_dir2($file){$file_stats = stat($file);if(FileHelper::windows_server())return is_dir($file) || strpos($file,".")===false&&in_array($file_stats[7],array(0,4096));else return is_dir($file);}

    static function windows_server(){ return in_array(strtolower(PHP_OS), array("win32", "windows", "winnt"));}

    static function destroy($filename, $type = 'posts'){
        $path = dirname(__FILE__) . "/../../../public/uploads/" . $type . "/";
        foreach(FileHelper::$config as $id => $values){
            $fn = FileHelper::get_resized($filename,$id);
            if( is_file( $path . $fn ) ){
                unlink($path . $fn);
            }
        }
    }

    static function get_resized($path,$name){
        return str_replace(array(
            '.jpg',
            '.jpeg',
            '.png',
            '.gif'
        ),array(
            '-' . $name . '.jpg',
            '-' . $name . '.jpeg',
            '-' . $name . '.png',
            '-' . $name . '.gif'
        ),strtolower($path));
    }

       static function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
    }

    static function resize_group($name, $destinationPath,$filename){

        foreach(FileHelper::$config as $id => $value){
            $values = explode("x",$value);
            $w = $values[0];
            $h = $values[1];
            $fn = FileHelper::get_resized($filename,$id);
            $wm = 0;

            if(in_array($id,array('wd','hd'))){
                $wm = 1;
            }

            if( $wm && is_file( dirname(__FILE__) . '/../../../public/assets/img/watermark/' . $id . '.png' )){
                Image::make(Input::file($name)->getRealPath())
                    ->fit((int)$w,(int)$h)
                    ->insert( dirname(__FILE__) . '/../../../public/assets/img/watermark/' . $id . '.png')
                    ->save($destinationPath . $fn, 100);
            } else {
                Image::make(Input::file($name)->getRealPath())
                    ->fit((int)$w,(int)$h)
                    ->save($destinationPath . $fn, 100);
            }
        }

        // Input::file('image')->move($destinationPath, $filename);
    }

    static function getRandomImage(){
        //$available = FileHelper::scandir( './assets/img/formbgs', 1, 'jpg jpeg png fig' );
        //return $available[rand(0,count($available)-1)];
    }
}