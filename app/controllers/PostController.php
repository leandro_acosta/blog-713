<?php namespace App\Controllers;

use App\Models\Post;
use Tag, PostTag;

class PostController extends \BaseController {

    public function index(){

        $posts = Post::paginate(5);

        return \View::make('posts',array(
            'posts' => $posts
        ));
    }

    public function show($id){

        $post = Post::visible()
            ->find($id);
        if ($post) {
            
        return \View::make('post',array(
            'post' => $post
            ));
        }
        return \View::make('error.missing');
    }
    public function home(){

    $posts = Post::visible()
        ->orderBy('updated_at','desc')
        ->take(3)
        ->get();

    return \View::make('home',array(
        'posts' => $posts
    ));
    }

    public function search()
    {  

        /*
        $s = Input::get('s');
        return View::make('results', array(
            'entries'=> Post::where('title','like','%'.$s.'%')
                ->orWhere('caption','like','%'.$s.'%')
                ->orWhere('body', 'like', '%'.$s.'%')
                ->orderBy('updated_at','desc')
                ->paginate(10),
            'recents' => Post::visible()
                ->orderBy(DB:raw('RAND()'))
                ->take(5)
                ->get()
        )); 
        */
    }
    public function tags($tag){
        $tag_id = Tag::where('tag',$tag)
            ->pluck('id');

        if ($tag_id) {
            $posts_ids = PostTag::where('tag_id', $tag_id)
                ->lists('posts_ids');
            
            if ($posts_ids) {
                $posts = Post::visible()
                    ->whereIn('id', $posts_ids)
                    -paginate(10);
                
                if ($posts) {
                    return \View::make('tags', array(
                        'posts' => $posts
                    ));           
                }
            }
        }
        return \View::make('errors.missing');
    }
}