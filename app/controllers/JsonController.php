<?php namespace App\Controllers;

use Illuminate\Filesystem\Filesystem;
use DB,
    Request,
    Input,
    Sentry,
    Str,
    Url,
    File,
    Response,
    Session,
    Post,
    PostCategory,
    View,
    User,
    PostImage,
    FileHelper,
    StrHelper;

class JsonController extends \BaseController {

    public function show($id, $args = array())
    {
        $method = str_replace(".","_",implode('_',array_values(array_filter(array(Request::segment(2),Request::segment(3))))));
        if(method_exists(get_called_class(), $method)){
            return call_user_func_array(array($this, $method), $args);
        }
    }

    public function post_images_json()
    {
        $input = Input::all();
        $entry = Post::find($input['id']);

        return Response::json($entry->images->toArray());
    }

    public function post_tags_json()
    {
        $input = Input::all();
        $entry = Post::find($input['post_id']);

        return $entry->tags->toArray();
    }    
}