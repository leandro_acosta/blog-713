<?php namespace App\Controllers\Admin;

use App\Models\Post;
use App\Models\Tag;
use App\Models\PostTag;
use App\Services\Validators\PostValidator;

class PostController extends \BaseController {

    public function index(){

        Post::where('title',"")
            ->delete();

        $posts = Post::orderBy('id','desc')
            ->paginate(10);

        return \View::make('admin.posts.index',array(
            'posts' => $posts
        ));
    }

    public function create(){

        $post = new Post();
        $post->status_id = 0;
        $post->save();

        $id = \DB::getPdo()->lastInsertId();    

        return \View::make('admin.posts.edit', array(
            'entry' => Post::find($id),
            //'category_options' => Category::lists('title','id')
        ));
    }

    public function edit($id){
        return \View::make('admin.posts.edit')
            ->with('entry', Post::find($id));
    }

    public function update($id){
        $validation = new PostValidator;

        if ($validation->passes())
        {        
            $post = Post::find($id);
            $post->title   = \Input::get('title');
            $post->slug    = \Str::slug(\Input::get('title'));
            $post->caption    = \Input::get('caption');
            $post->body    = \Input::get('body');
            $post->user_id = \Sentry::getUser()->id;
            $post->status_id  = \Input::get('status_id') == 'on' ? 1 : 0;
            $post->comments  = \Input::get('comments') == 'on' ? 1 : 0;
            $post->home  = \Input::get('home') == 'on' ? 1 : 0;
            $post->publish_start = \Input::get('publish_start');
            $post->publish_end = \Input::get('permanent') ? '0000-00-00 00:00:00' :  \Input::get('publish_end');
            $post->save();

            //\Notification::success('El Artículo <strong>' . $post->title . '</strong> fue actualizado correctamente.');

            return \Redirect::route('admin.posts.index')->with("success","Creado / Actualizado");
        }

        //dd($validation->errors);
          return \Redirect::back()->withInput()->withErrors($validation->errors);
    }

    public function destroy($id)
    {
        $entry = Post::find($id);
        $entry->delete();

        return \Redirect::route('admin.posts.index')
            ->with("success","Borrado");
    }

    public function tags_all(){
        $post_id = \Input::get('post_id');
        $user_id = \Sentry::getUser()->id;
        $tags = Tag::where('user_id',$user_id)
            ->get();
        $included = [];
        $excluded = [];


        foreach($tags as $tag){

            $exists = PostTag::where('tag_id',$tag->id)
                ->where('post_id',$post_id)
                ->count();

            if( $exists ) {
                $included[] = $tag->tag;
            } else {
                $excluded[] = $tag->tag;
            }
        }

        return array(
            'included' => $included,
            'excluded' => $excluded
        );
    }

    public function tags_relation_add(){
        $tags = \Input::get('tags');
        $post_id = \Input::get('post_id');

        foreach($tags as $tag){

            $tag_id = Tag::where('tag',$tag)
                ->where('user_id',\Sentry::getUser()->id)
                ->pluck('id');

            // si no existe lo guardamos
            if( ! $tag_id ){
                $entry = new Tag;
                $entry->tag = $tag;
                $entry->user_id = \Sentry::getUser()->id;
                $entry->save();

                $tag_id = \DB::getPdo()->lastInsertId();    
            } 

            // establecemos la relación
            $post_tag = new PostTag;
            $post_tag->tag_id = $tag_id;
            $post_tag->post_id = $post_id;
            $post_tag->save();
        }

        // devolvemos los tags para mostrarlos
        return $tags;

    }

    public function tags_relation_remove(){
        $tags = \Input::get('tags');
        $post_id = \Input::get('post_id');

        foreach($tags as $tag){

            $tag_id = Tag::where('tag',$tag)
                ->pluck('id');

            // si existe lo volamo' borrando la relación
            if( $tag_id ){
                PostTag::where('tag_id',$tag_id)
                    ->delete();
            }
        }

        // devolvemos los tags para mostrarlos
        return $tags;

    }

}

