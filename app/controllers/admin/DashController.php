<?php namespace App\Controllers\Admin;

use Input, 
  View, 
  Redirect, 
  Sentry, 
  Post,
  Notification, 
  User;

class DashController extends \BaseController {

    public function index()
    {
    return View::make('admin.dash',array(
      'post_count'  => Post::all()->count(),
      'user_count'  => User::all()->count()
    ));
  }
}