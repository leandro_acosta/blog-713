<?php namespace App\Controllers;

use Request,
    Input,
    Str,
    File,
    Sentry,
    Response,
    View,
    User,
    Image,
    StrHelper,
    PostImage,
    FileHelper;

class ImageController extends \BaseController {


    protected static $uploaddir = 'uploads/';

    public function upload(){

        $data = Input::all();
        extract($data);

        $ds          = DIRECTORY_SEPARATOR;  //1

        $storeFolder = self::$uploaddir;   //2
        $storeFolder.= Input::get('tag','');

        if ( ! empty($_FILES) ) {

            $upload = new $scheme();
            $i = $scheme::where('parent_id',$parent_id)
                ->count();

            $path = base_path().'/public';
            $file = Input::file('file');

            $destinationPath = $path . $ds. $storeFolder . $ds; // . $_FILES['file']['name'];
            $filename = str_replace(' ','-',strtolower(StrHelper::sanitize($file->getClientOriginalName(),Input::get('parent'))));

            $filename = ImageController::make_unique_filename($filename,$destinationPath);

            $upload->name = $filename;
            $upload->sorted = $i+1;
            $upload->file_size = $file->getSize();
            $upload->mime_type = $file->getMimeType();
            $upload->parent_id = $parent_id;
            $upload->save();

            FileHelper::resize_group('file',$destinationPath,$filename);

            return Response::json(array(
                'success' => true, 
                'scheme' => $scheme,
                'tag' => $tag,
                'id' => $upload->id, 
                'parent_id' => $parent_id)
            , 200);
            //move_uploaded_file($tempFile,$targetFile); //6
        }
    }

    protected function simpleupload(){ 
        if ($_FILES['file']['name']) {
            if (!$_FILES['file']['error']) {
                $path = base_path().'/public/uploads/';
                $y = date('Y');
                $m = date('m');
                $fsy = $path . $y;
                $folder = $y.'/'.$m;
                $fs = $path . $folder;
                if(!is_dir($fsy)){
                    mkdir($fsy,777);
                }                
                if(!is_dir($fs)){
                    mkdir($fs,777);
                }
                $name = md5(rand(100, 200));
                $ext = explode('.', $_FILES['file']['name']);
                $filename = $name . '.' . $ext[1];
                $destination = $fs . $filename; //change this directory
                $location = $_FILES["file"]["tmp_name"];
                move_uploaded_file($location, $destination);
                $message =  '/uploads/' . $folder . $filename;//change this URL
            }
            else
            {
              $message = 'Ooops!  Your upload triggered the following error:  '.$_FILES['file']['error'];
            }
        }

        return Response::json($message);
    }

    protected function make_unique_filename($filename, $destination)
    {
        $i = 0;
        $path_parts = pathinfo($filename);
        $path_parts['filename'] = Str::slug($path_parts['filename'], '-');
        $filename = $path_parts['filename'];

        while (File::exists($destination.$filename.'-th.'.$path_parts['extension'])) {
            $filename = $path_parts['filename'].'-'.$i;
            $i++;
        }

        return $filename.'.'.$path_parts['extension'];
    }

    public function destroy(){
        $input = Input::all();
        extract($input);
        $entry = $scheme::find($id);
        if($entry){
            FileHelper::destroy($entry->name,$tag);
            $entry->delete();
            return Response::json(array('ok'));
        } else {
            return Response::json(array('notfound'));
        }
    }

    public function ordering(){

        $input = Input::all();
        $scheme = $input['scheme'];

        if($input['sorted']){
            foreach($input['sorted'] as $i => $id){
                $entry = $scheme::find($id);
                $entry->sorted = $i+1;
                $entry->save();
            }
        }

        return Response::json(array('ok'));
    }


    public function search() {

        $filter = Input::all();
        $fields = array('type_id','maker_id','fuel_id');
        $str = 'Vehicle::with("image")';
        $json = array();

        foreach($filter as $key => $value){
            if($value && in_array($key,$fields)){
                $str .= '->where("' . $key . '", "=", "' . $value . '")';
            }
        }

        $str .= '->orderBy("id", "desc")->get();';

        eval("\$results = $str;");

        if($results){
            foreach($results as $entry){

            }
        }
        return $results;
    }
}