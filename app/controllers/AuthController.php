<?php namespace App\Controllers;

use Response, 
    Request, 
    Input, 
    Redirect, 
    Sentry, 
    View, 
    Notification;

class AuthController extends \BaseController {

  public function logout()
  {
    Sentry::logout();

    return Redirect::to('/session-ended');
  }        

  public function login()
  {

    $credentials = array(
      'email'    => Input::get('email'),
      'password' => Input::get('password')
    );

    try
    {

      if(Input::get('remember-me') == 'on')
      {
        $user = Sentry::authenticateAndRemember($credentials);
      }
      else
      {
        $user = Sentry::authenticate($credentials, false);        
      }

      $grpAdmin = Sentry::getGroupProvider()->findByName('Admin');

      if(Sentry::check() && Input::get('redirect_to'))
      {
        return Redirect::to(Input::get('redirect_to'));
      }
      if ($user->inGroup($grpAdmin))
      {
        return Redirect::to('admin');
      }
      else
      {
        return Redirect::to('login')
          ->withErrors(array(
              'login' => "Lo lamento, su usuario no corresponde a un grupo válido."
            ));
      }
    }
    catch(\Exception $e)
    {
      return Redirect::to('login')
        ->withErrors(array(
            'login' => $e->getMessage()
          ));
    }
  }
}