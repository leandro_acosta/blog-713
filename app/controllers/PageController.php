<?php namespace App\Controllers;

class PageController extends \BaseController {

    public function show($slug){

        $page = 'pages.' . $slug;

        if( ! \View::exists($page) ) {
            return \View::make('errors.missing');
        }

        return \View::make($page);
    }    
}