$(function($){
  if( $('.slick').length ){
    $('.slick').slick({
      dots: true,
      infinite: true,
      speed: 500,
      autoplaySpeed: 1000,
      autoplay:true,
      arrows: false,
      fade: true
    });
  }
 
  if( $('.slick-nav').length ){
 
    var navlength = $('.slick-nav').children().length;
 
    $('.slick-nav').slick({
        asNavFor: '.slick',
        centerMode: false,
        centerPadding: '60px',
        autoplay: true,
        slidesToShow: 6,
        responsive: [
          {
            breakpoint: 768,
            settings: {
              arrows: true,
              centerMode: false,
              centerPadding: '40px',
              slidesToShow: 4
            }
          },
          {
            breakpoint: 480,
            settings: {
              arrows: true,
              centerMode: false,
              centerPadding: '40px',
              slidesToShow: 2
            }
          }
        ],
        focusOnSelect: true
    });
  }
 
});