Dropzone.autoDiscover = false;
/* Post Tags */

$(document).on('click','.btn-tag-included',function(e){
  e.preventDefault();
  tags_exclude([$(this).text()],this);
});

$(document).on('click','.btn-tag-excluded',function(e){
  e.preventDefault();
  tags_include([$(this).text()],this);
});

function tags_add(){
  var tags_str = $.trim($( "#tags-add" ).val());
console.log($( "#tags-add" ));
  if(tags_str == ""){
    alert("No string!");
    return false;
  }

  var tags = tags_str.split(",");

  for(var i in tags){
    tags[i] = $.trim(tags[i]);
  }
  $( "#tags-add" ).val( "" );
  tags_include(tags);
}

function tags_exclude(tags,btn){
    var post_id = $('input[name="id"]').val();
  $.ajax({
    type:"POST",
    url: "/admin/tags_relation_remove",
    data: {
      post_id : post_id,
      tags : tags
    },
    success : function(json){
        if(json){
            $(json).each(function(i,tag){
                $( ".tags-excluded" ).append( '<a href="#" class="label label-info btn-tag-excluded">' + tag + '</a>' );
            });
            if($(btn).length){
                $(btn).remove();
            }
        }
    }
  });
}

function tags_include(tags,btn){
    var post_id = $('input[name="id"]').val();
  $.ajax({
    type:"POST",
    url: "/admin/tags_relation_add",
    data: {
      post_id : post_id,
      tags : tags
    },
    success : function(json){
        if(json){
            $(json).each(function(i,tag){
                $( ".tags-included" ).append( '<a href="#" class="label label-success btn-tag-included">' + tag + '</a>' );
            });
        }
        if($(btn).length){
            $(btn).remove();
        }
    }
  });
}

function tags_all(){
    var post_id = $('input[name="id"]').val();
  $.ajax({
    type:"POST",
    url: "/admin/tags_all",
    data: {
      post_id : post_id
    },
    success : function(json){
      if(json){
        if(json.included){
          $(json.included).each(function(i,tag){
            $( ".tags-included" ).append( '<a href="#" class="label label-success btn-tag-included">' + tag + '</a>' );
          });
        }
        if(json.excluded){
          $(json.excluded).each(function(i,tag){
            $( ".tags-excluded" ).append( '<a href="#" class="label label-info btn-tag-excluded">' + tag + '</a>' );
          });
        }
      }
    }
  });
}



function post_tags(post_id){
  $.ajax({
    type:"get",
    url: "/json/post_tags.json",
    data: {
      post_id : post_id
    },
    success : function(json){
      if(json){
        $(json).each(function(i,tag){
          $( ".post-tags" ).append( '<button class="btn btn-success">' + tag + '</button>' );
        });
      }
    }
  });
}

function get_resized(name,type,scope){
  name = name.toLowerCase();
  name = name.replace('.jpg', '-' + type + '.jpg');
  name = name.replace('.jpeg','-' + type + '.jpeg');
  name = name.replace('.png', '-' + type + '.png');
  name = name.replace('.gif', '-' + type + '.gif');

  return "/uploads/" + scope + "/" + name;
}


function sendFile(file, editor, welEditable) {
    data = new FormData();
    data.append("file", file);
    $('.summernote-progress').removeClass('hide').hide().fadeIn();
    $.ajax({
        data: data,
        type: "POST",
        xhr: function() {
            var myXhr = $.ajaxSettings.xhr();
            if (myXhr.upload) myXhr.upload.addEventListener('progress',progressHandlingFunction, false);
            return myXhr;
        },        
        url: "/api/upload_single",
        cache: false,
        contentType: false,
        processData: false,
        success: function(url) {
          $('.summernote-progress').fadeOut();
          editor.insertImage(welEditable, url);
        }
    });
}   

function progressHandlingFunction(e){
    if(e.lengthComputable){
        var perc = Math.floor((e.loaded/e.total)*100);
        $('.progress-bar').attr({"aria-valuenow":perc}).width(perc+'%');
        // reset progress on complete
        if (e.loaded == e.total) {
            $('.progress-bar').attr('aria-valuenow','0.0');
        }
    }
}

$(function($){

    if( $('.tags-list').length ){
        tags_all();
    }

    if( $('.btn-tags-add').length ){
        $('.btn-tags-add').click(function(e){
            e.preventDefault();
            tags_add();
        });
    }

    if( $('.summernote').length ){
        $('.summernote').summernote({
            height: 200,   
            minHeight: null,
            maxHeight: null,
            onImageUpload: function(files, editor, welEditable) {
              sendFile(files[0], editor, welEditable);
            }
        });
    }

    if( $('.datetimepicker').length ){
        $('.datetimepicker').datetimepicker({
        format: "YYYY-MM-DD HH:mm:ss",
        language: "es"
      });
    }

    if( $('#form_dropzone').length ){
        var myDropzoneOptions = {
            maxFilesize: 10,
            addRemoveLinks :  true,
            clickable: true,
            dictDefaultMessage: "Arrastra ficheros aquí para cargar",
            dictFallbackMessage: "Tu navegador no soporta drag and drop.",
            dictFallbackText: "Please use the fallback form below to upload your files like in the olden days.",
            dictFileTooBig: "Fichero demasiado grande ({{filesize}}MiB). Tamaño máximo: {{maxFilesize}}MiB.",
            dictInvalidFileType: "No puedes cargar ficheros de este tipo.",
            dictResponseError: "Server responded with {{statusCode}} code.",
            dictCancelUpload: "Cancelar",
            dictCancelUploadConfirmation: "Estás seguro que deseas cancelar la carga?",
            dictRemoveFile: "Eliminar",
            acceptedFiles: 'image/*', 
            init: function() {

                this.on("sending", function(file, xhr, formData) {
                    formData.append("scheme", $('#form_dropzone').data('scheme') );
                    formData.append("tag", $('#form_dropzone').data('tag') );
                    formData.append("parent_id", $('#form_dropzone').data('id') ); 
                });

                this.on("success", function(file, response) {
                    file.serverId = response.id;
                    $(file.previewElement).data('id',response.id);
                    $(file.previewElement).data('parent-id',response.parent_id);            
                    $(file.previewElement).data('scheme',response.scheme);
                    $(file.previewElement).data('tag',response.tag);
                });

                this.on("removedfile", function(file) {
                    if (!$(file.previewElement).data('id')) { return; }
                    $.ajax({
                        type: 'POST',
                        url: '/image/destroy',
                        data: {
                            id:$(file.previewElement).data('id'),
                            tag:$(file.previewElement).data('tag'),
                            scheme:$(file.previewElement).data('scheme')
                        }
                    });
                });
            }    
        };

        var myDropzone = new Dropzone('#form_dropzone', myDropzoneOptions);

        $('#form_dropzone').sortable({
            update: function (event, ui) {
                var sorted=[];
                var id = "";
                var scheme = "";
                $(this).find('.dz-preview').each(function(i,item){
                    if(id==""){
                        id = $(item).data('parent-id');
                    }
                    if(scheme==""){
                        scheme = $(item).data('scheme');
                    }
                    sorted[i] = $(item).data('id');
                });

                $.ajax({
                    type:"POST",
                    url: "/image/ordering",
                    data: {id:id,sorted:sorted,scheme:scheme},
                    success : function(json){
                        $('.bubble').removeClass('hide').hide().fadeIn('fast',function(){
                            var $this = $(this);
                            setTimeout(function(){
                                $this.fadeOut();
                            },500)
                        });
                    }
                });
            }
        });


        var url = $('.dropzone').data("url");
        var id = $('.dropzone').data("id");
        var tag = $('.dropzone').data("tag");
        var scheme = $('.dropzone').data("scheme");

        $.ajax({
            type:'get',
            url:url,
            data : {
                id:id
            },
            success : function(json){
                $(json).each(function(i,item){

                    var mockFile = { name: item.name, size: item.file_size };

                    myDropzone.emit("addedfile", mockFile);
                    myDropzone.emit("thumbnail", mockFile, get_resized(item.name,'th',tag));

                    $(mockFile.previewElement).data('id',item.id);
                    $(mockFile.previewElement).data('tag',tag);
                    $(mockFile.previewElement).data('scheme',scheme);
                    $(mockFile.previewElement).data('parent-id',item.parent_id);

                });
            }
          });
    }
});